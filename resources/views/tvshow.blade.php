<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TV Shows</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            
            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .subtitle {
                font-size: 34px;
            }

            .m-t-md {
                margin-top: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    TV Shows
                </div>

                <div class="row">
                    <form method="GET" action="{{ route('tvshow') }}">
                        <div class="row">
                            <div class="col-12">
                                <input id="q" type="text" class="form-control" name="q" value="" placeholder="Search TV Show" required autocomplete="off" autofocus>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn">
                                    {{ __('SEARCH') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="row">
                    @if (!empty($results))
                    <div class="subtitle m-t-md">
                        Results for: <b>"{{ $search }}"</b>
                    </div>
                    @foreach ($results as $result)
                    {{ $result }}
                    <br>
                    @endforeach
                    @else
                    <div class="subtitle m-t-md">
                        Nothing found for: <b>"{{ $search }}"</b>
                    </div>
                    @endif
                    @if (!empty($others))
                    <div class="m-t-md">
                        <b>Similar results:</b>
                    </div>  
                    @foreach ($others as $other)
                    {{ $other }} <br>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>
