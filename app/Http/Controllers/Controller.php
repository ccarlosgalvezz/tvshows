<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('welcome');
    }
    public function tvshow(Request $request)
    {
        // Gross request
        $search = $request->q;
        if (!empty($search)) {
            $json = file_get_contents("https://api.tvmaze.com/search/shows?q=".$search);
            $data = json_decode($json, true);
        }else{
            return redirect('/')->with('status', 'Please, insert any word!');
        }

        // Filter values
        $results = array();
        $others = array();
        foreach ($data as $k => $v) {
            if (strtolower($v['show']['name']) == strtolower($search)) {
                $results[$k] = $v['show']['name'];
            }
            else {
                $others[$k] = $v['show']['name'];
            }
        }

        // Show results
        return view('tvshow', compact('search', 'results', 'others'));
    }
}
