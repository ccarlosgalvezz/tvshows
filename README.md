This JSON API is built in Laravel 7.30.4 and does not need any special configuration to work, apart from server requirements (PHP 7.2.5, etc).
No database is needed as data comes from an external API.

First step is requesting data to the external API with the searching words from the form. Raw data is stored and filtered in order to get coincidences. In order to get this technical task requirements, comparisons are lowercase made in order to get non-case sensitive matches. Any empty search will be redirected to home.

Suggestions for future releases:
- Predictive searching
- Add filters as searching option
- Improve views
- Include api version in URL (routes/api.php file)
...

